from wtforms import Form, StringField, PasswordField, validators, SelectField
from wtforms.fields.html5 import EmailField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from models import db, Categoria, Termino, Medio
from flask_wtf.file import FileField, FileRequired
from werkzeug.utils import secure_filename

def get_categs():
    return Categoria.query.all()
def get_terms():
    return Termino.query.all()
def get_medios():
    return Medio.query.all()

class loginForm(Form):
	user = StringField('Username', [
		validators.Required(message="El username es requerido"),
		validators.length(min=4, max=25, message="Ingrese un nombre de usuario (min 4 caracteres)")
		])
	passwd = PasswordField('Password', [
		validators.Required(message="El password es requerido"),
		validators.length(min=8, max=25, message="Ingrese un password (min 8 caracteres)")
		])
	#def validate

class categoryForm(Form):
	nombre = StringField('Nombre', [
		validators.Required(message="El nombre es obligatorio")
		])

class medioForm(Form):
	nombre = StringField('Nombre', [
		validators.Required(message="El nombre es obligatorio")
		])
	url = StringField('Url')


class terminoForm(Form):
	nombre = StringField('Nombre', [validators.Required(message="El nombre es obligatorio")])
	categorias = QuerySelectField('Categorias', query_factory=get_categs)
	terminoPadre = QuerySelectField('Termino Padre', query_factory=get_terms, allow_blank=True)
	#categorias = SelectField('Categoria', coerce=int, choices=[Categoria.query.all()] )

class imagenForm(Form):
	#url = StringField('Nombre', [validators.Required(message="La url es obligatoria")])
	categorias = QuerySelectField('Categorias', query_factory=get_categs)
	foto = FileField()

class editimagenForm(Form):
	categorias = QuerySelectField('Categorias', query_factory=get_categs)
	foto = FileField()

class userForm(Form):
	nombre = StringField('Nombre', [
		validators.Required(message="El nombre es obligatorio")
		])
	termino = QuerySelectField('Terminos', query_factory=get_terms, allow_blank=True)
	passwd = PasswordField('Password (deje en blanco si no quiere cambiar el password)', [
		validators.Required(message="El password es requerido"),
		validators.length(min=8, max=25, message="Ingrese un password (min 8 caracteres)")
		])
	tipoPlan = SelectField('', choices=[('1', 'Básico'), ('5','Middle'), ('10', 'Full')])

class usermodifForm(Form):
	nombre = StringField('Nombre', [
		validators.Required(message="El nombre es obligatorio")
		])
	termino = QuerySelectField('Terminos', query_factory=get_terms, allow_blank=True)
	passwd = PasswordField('Password (deje en blanco si no quiere cambiar el password)')
	tipoPlan = SelectField('', choices=[('1', 'Básico'), ('5','Middle'), ('10', 'Full')])

class excluyenteForm(Form):
	nombre = StringField('Nombre', [validators.Required(message="El nombre es obligatorio")])
	termino= QuerySelectField('Termino', query_factory=get_terms)
	
class medioexcluyenteForm(Form):
	termino= QuerySelectField('Termino', query_factory=get_terms)
	medio= QuerySelectField('Medio', query_factory=get_medios)
	
