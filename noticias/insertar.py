from models import db, User, Historico, Categoria, Medio, Termino, Foto, MedioExcluyente, Excluyente
from werkzeug.security import generate_password_hash


def insertar_usuario(username, password):
	user = User(username, password)
	user.nivel = 100
	db.session.add(user)

	db.session.commit()

def ins_modif_user(username, terminos, password='', id=None):

	u = User.query.filter_by(id=id).first()
	if u == None:

		user = User(username=username, password=password)
		aux = ''

		for t in terminos:
			taux = Termino.query.filter_by(nombre=t).first()
			aux += str(taux.idterminos)+","

		aux = aux[:-1]

		user.termino = aux
		user.nivel = 10

	else:

		user = User.query.filter_by(id =id).first()
		user.username = username
		if password != '' and password != None:
			user.password = generate_password_hash(password)
		aux = ''
		index = 0

		for t in terminos:
			taux = Termino.query.filter_by(nombre=t).first()
			if user.nivel < 10 and taux.categoria != 25:
				index += 1
			aux += str(taux.idterminos)+","

		if user.nivel == 5 and index > 100:
			print('El nivel es muy bajo, nivel 5') 
			raise NameError('El nivel es muy bajo, nivel 5') 
			
		if user.nivel == 1 and index > 0:
			print('El nivel es muy bajo, nivel 1') 
			raise NameError('El nivel es muy bajo, nivel 1') 

		aux = aux[:-1]

		user.termino = aux

	db.session.add(user)
	db.session.commit()
	#return redirect(url_for('index'))   

def insertar_noticia(titulo, bajadaNoticia, link, medioOrigen, termino, fecha, link_foto=''):
	if titulo != "" and titulo != None:
		
		noticia = Historico.query.filter( (Historico.titulo == titulo) | (Historico.bajadaNoticia == bajadaNoticia) ).first()
		
		if noticia != None:
			#modificar
			noticia.titulo = titulo.strip()
			noticia.bajadaNoticia = bajadaNoticia.strip()
			noticia.link = link
			noticia.link_foto = link_foto
			noticia.medioOrigen = medioOrigen
			#if categoria != '' and categoria != None:
			#	noticia.categoria = categoria

			noticia.termino = termino
			noticia.fecha = fecha
			db.session.add(noticia)
			db.session.commit()
		else:
			noticia = Historico(titulo=titulo.strip(), bajadaNoticia=bajadaNoticia.strip(), link= link, medioOrigen= medioOrigen, termino=termino, fecha=fecha, link_foto=link_foto)
			db.session.add(noticia)
			db.session.commit()

def ins_modif_categoria(nombre, id =None):
	c = Categoria.query.filter_by(id=id).first()
	if c != None:
		#modificar
		c.nombre_categoria = nombre
		db.session.add(c)
		db.session.commit()
	else:
		c = Categoria(nombre_categoria=nombre)
		db.session.add(c)
		db.session.commit()

def ins_modif_medio(nombre, url, id =None):
	m = Medio.query.filter_by(id=id).first()
	if m != None:
		#modificar
		m.nombre = nombre
		m.url = url
		db.session.add(m)
		db.session.commit()
	else:
		m = Medio(nombre=nombre, url=url)

		db.session.add(m)
		db.session.commit()

def ins_modif_imagen(url, foto, categoria, id =None):
	f = Foto.query.filter_by(idfotos=id).first()
	if f != None:
		#modificar
		if url != None:
			f.url = url
		if foto != None:
			f.foto = foto
		if categoria != '':
			f.categoria = categoria.id

		db.session.add(f)
		db.session.commit()
	else:
		if categoria != '':
			categoria_id = None
		else:
			categoria_id = categoria
		f = Foto(categoria=categoria_id, url=url, foto = foto)
		db.session.add(f)
		db.session.commit()

def ins_modif_termino(nombre, categoria, termino=None, id = None):
	t = Termino.query.filter_by(idterminos=id).first()
	if t != None:
		#modificar
		t.nombre = nombre
		t.categoria = categoria.id
		tp = Termino.query.filter_by(nombre=termino).first()
		if tp != None:
			t.padre = tp.idterminos
		db.session.add(t)
		db.session.commit()
	else:
		t = Termino(nombre=nombre, categoria=categoria.id, padre=termino.idterminos)
		db.session.add(t)
		db.session.commit()

	ter = t.idterminos
	hist = Historico.query.filter_by(termino=None)
	for h in hist:
		try:
			if t.nombre.lower() in h.titulo.lower() or t.nombre.lower() in h.bajadaNoticia.lower():			
				excluyentes = Excluyente.query.filter(termino=t.idterminos)
				for e in excluyentes:
					if e.nombre.lower() in h.titulo.lower() or e.nombre.lower() in h.bajadaNoticia.strip().lower():			
						ter = None

					mediosexcl = MedioExcluyente.query.filter(termino=ter, medioOrigen= medioOrigen).first()
					if mediosexcl != None:
						ter = None


			h.termino = ter
			db.session.add(h)
			db.session.commit()
		except AttributeError:
			pass

def ins_modif_excluyente(nombre, termino, id =None):
	m = Excluyente.query.filter_by(id=id).first()
	if m != None:
		#modificar
		m.nombre = nombre
		t = Termino.query.filter_by(nombre=termino).first()
		if t != None:
			m.termino = t.idterminos
		db.session.add(m)
		db.session.commit()

	else:
		t = Termino.query.filter_by(nombre=str(termino)).first()
		m = Excluyente(nombre=nombre, termino=t.idterminos)
		#modificar la noticia que tenga este termino Y que tenga en el titulo o la descripcion el nombre
		db.session.add(m)
		db.session.commit()

	noticias = Historico.query.filter(termino=t)
	noticias = noticias.filter(( func.lower(Historico.titulo) == func.lower(nombre)) | ( func.lower(Historico.bajada) == func.lower(nombre) ))
	for n in noticias:
		n.termino = None
		db.session.add(n)
		db.session.commit()

def ins_modif_medio_excluyente(nombre, termino, id =None):
	m = MedioExcluyente.query.filter_by(id=id).first()
	if m != None:
		#modificar
		t = Termino.query.filter_by(nombre=str(termino)).first()
		x = Medio.query.filter_by(nombre=str(nombre)).first()
		if t != None:
			m.termino = t.idterminos
		if x != None:
			m.medioOrigen = x.id
		db.session.add(m)
		db.session.commit()
	else:
		t = Termino.query.filter_by(nombre=str(termino)).first()
		x = Medio.query.filter_by(nombre=str(nombre)).first()
		
		m = MedioExcluyente(medioOrigen=x.id, termino=t.idterminos)

		db.session.add(m)
		db.session.commit()

	noticias = Historico.query.filter(termino=t)
	noticias = noticias.filter(medioOrigen=m)
	for n in noticias:
		n.termino = None
		db.session.add(n)
		db.session.commit()
