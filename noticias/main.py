from flask import Flask, render_template, request, session, url_for, redirect, send_file
from flask_wtf import CSRFProtect
from werkzeug.utils import secure_filename
from werkzeug.exceptions import BadRequestKeyError
import forms
from config import DevelopmentConfig
from models import db, User, Termino, Historico, Categoria, Medio, Foto, Excluyente, MedioExcluyente
from insertar import insertar_usuario, ins_modif_categoria, ins_modif_medio, ins_modif_termino, ins_modif_imagen, ins_modif_user, ins_modif_excluyente, ins_modif_medio_excluyente
from scraping import getHeadlines, get_bolsa_comercio_esencial, get_bolsa_comercio_noticias, get_emol, get_elpulso, get_bbc, get_wsj, get_nytimes, encontrar_termino, asignar_imagen
import os, sys, base64, json, datetime
from io import BytesIO
from zipfile import ZipFile
from sqlalchemy import func

app = Flask(__name__)
#app = Flask(__name__, template_folder = )
#app.secret_key = "iUZqQFHNntuy2AJHTMuS"

app.config.from_object(DevelopmentConfig)

csrf = CSRFProtect(app)

@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404

@app.route('/', methods=['GET', 'POST'])
def index():
    if 'user' in session:
        user = User.query.filter(func.lower(User.user)==func.lower(session['user'])).first()
        print(session['user'])
        return render_template('perfil.html', user= user)
    else:
        login_form = forms.loginForm(request.form)
        if request.method == 'POST' and login_form.validate():
            username =login_form.user.data
            passwd =login_form.passwd.data

            user = User.query.filter(func.lower(User.user)==func.lower(username)).first()
            if user is not None and user.verify_password(passwd):
                session['user'] = username.lower()
                return render_template('perfil.html', user= user)

            else:
                print("Usuario o password no validos")
                return redirect(url_for('index'))
        if request.method == 'GET':
            pass
        return render_template('index.html', form = login_form)

@app.route('/logout')
def logout():
    if 'user' in session:
        session.pop('user')
        return redirect(url_for('index'))

#@app.route('/insertar')
#def insertar():
#   insertar_usuario('felipe', 'felipe.-')
    #noticia.query.join().paginate(n,n,False)
#    pass
#    return redirect(url_for('index'))

@app.route('/termino/borrar/<int:id>/')
def borrar_termino(id):
    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:
            termino = Termino.query.filter_by(idterminos=id).first()
            db.session.delete(termino)
            db.session.commit()
            return redirect(url_for('get_termino'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/termino/')
@app.route('/termino/<id>/',  methods=['GET', 'POST'])
def get_termino(id = ''):

    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:

            if id == '':
                termino = Termino.query
                filter_id = request.args.get('filter_id', '')
                filter_nombre= request.args.get('filter_nombre', '')
                filter_categoria= request.args.get('filter_categoria', '')
                
                if filter_categoria != '':
                    termino = termino.join(Categoria).filter(Categoria.nombre_categoria.like('%'+filter_categoria+'%'))                
                if filter_nombre != '':
                    termino = termino.filter(Termino.nombre.like('%'+filter_nombre+'%'))
                if filter_id != '':
                    termino = termino.filter_by(idterminos=filter_id)

                if termino == None:
                    return 'No existen términos asociados'
                else:
                    return render_template('termino/lista.html', terminos= termino, user= u, filter_id = filter_id, filter_nombre=filter_nombre, filter_categoria=filter_categoria)
            else:
                if id == 'add':
                    if request.method == 'GET':
                        form = forms.terminoForm(request.form)
                        termino = None
                        return render_template('termino/form.html', termino= termino, user= u, form = form)
                    if request.method == 'POST':
                        form = forms.terminoForm(request.form)
                        nombre =form.nombre.data
                        categorias =form.categorias.data
                        terminos =form.terminos.data
                        
                        ins_modif_termino(nombre, categorias, terminos)

                        #Ahora actualiza todas las noticias con ese termino
                        return redirect(url_for('get_termino'))
                
                termino = Termino.query.filter_by(idterminos=id).first()
                if request.method == 'GET':
                    
                    if termino == None:
                        return 'No existen terminos asociados'
                    else:
                        form = forms.terminoForm(request.form)
                        aux = termino.padre
                        dic_terminos=[]
                        if aux != None: 
                            for a in aux.split(','):
                                term = Termino.query.filter_by(idterminos=a).first()
                                dic_terminos.append(term)

                        return render_template('termino/form.html', termino= termino, user= u, form = form, dic_terminos=dic_terminos)
                if request.method == 'POST':
                    form = forms.terminoForm(request.form)
                    nombre =form.nombre.data
                    categorias =form.categorias.data
                    terminos =form.terminos.data
                    ins_modif_termino(nombre, categorias, terminos, termino.idterminos)
                        
                    return redirect(url_for('get_termino'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/excluyente/borrar/<int:id>/')
def borrar_excluyente(id):
    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:
            excluyente = Excluyente.query.filter_by(id=id).first()
            db.session.delete(excluyente)
            db.session.commit()
            return redirect(url_for('get_excluyente'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/excluyente/')
@app.route('/excluyente/<id>/',  methods=['GET', 'POST'])
def get_excluyente(id = ''):

    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:

            if id == '':
                excluyente = Excluyente.query
                filter_nombre= request.args.get('filter_nombre', '')
                filter_termino= request.args.get('filter_termino', '')
                
                if filter_termino != '':
                    excluyente = excluyente.join(Termino).filter(Termino.nombre.ilike('%'+filter_termino+'%'))                
                
                if filter_nombre != '':
                    excluyente = excluyente.filter(Excluyente.nombre.ilike('%'+filter_nombre+'%'))

                if excluyente == None:
                    return 'No existen palabras excluentes asociadas'
                else:
                    return render_template('excluyente/lista.html', terminos= excluyente, user= u, filter_nombre=filter_nombre, filter_termino=filter_termino)
            else:
                if id == 'add':
                    if request.method == 'GET':
                        form = forms.excluyenteForm(request.form)
                        excluyente = None
                        return render_template('excluyente/form.html', termino= excluyente, user= u, form = form)
                    if request.method == 'POST':
                        form = forms.excluyenteForm(request.form)
                        nombre =form.nombre.data
                        terminos =form.termino.data
                        
                        ins_modif_excluyente(nombre, terminos)

                        #Ahora actualiza todas las noticias con ese termino
                        return redirect(url_for('get_excluyente'))
                
                excluyente = Excluyente.query.filter_by(id=id).first()
                if request.method == 'GET':
                    
                    if excluyente == None:
                        return 'No existen palabras excluentes asociadas'
                    else:
                        form = forms.excluyenteForm(request.form)
                        
                        return render_template('excluyente/form.html', termino= excluyente, user= u, form = form)
                if request.method == 'POST':
                    form = forms.terminoForm(request.form)
                    nombre =form.nombre.data
                    terminos =form.terminos.data
                    ins_modif_termino(nombre, terminos, excluyente.id)
                        
                    return redirect(url_for('get_excluyente'))

        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/medio_excluyente/borrar/<int:id>/')
def borrar_medio_excluyente(id):
    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:
            medio_excluyente = MedioExcluyente.query.filter_by(id=id).first()
            db.session.delete(medio_excluyente)
            db.session.commit()
            return redirect(url_for('get_medio_excluyente'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/medio_excluyente/')
@app.route('/medio_excluyente/<id>/',  methods=['GET', 'POST'])
def get_medio_excluyente(id = ''):

    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:

            if id == '':
                medio_excluyente = MedioExcluyente.query
                filter_nombre= request.args.get('filter_nombre', '')
                filter_termino= request.args.get('filter_termino', '')
                
                if filter_termino != '':
                    medio_excluyente = medio_excluyente.join(Termino).filter(Termino.nombre.ilike('%'+filter_termino+'%'))                
                
                if filter_nombre != '':
                    medio_excluyente = medio_excluyente.join(Medio).filter(Medio.nombre.ilike('%'+filter_nombre+'%'))                
                
                    #medio_excluyente = medio_excluyente.filter(Excluyente.nombre.ilike('%'+filter_nombre+'%'))

                if medio_excluyente == None:
                    return 'No existen palabras excluentes asociadas'
                else:
                    return render_template('medio_excluyente/lista.html', terminos= medio_excluyente, user= u, filter_nombre=filter_nombre, filter_termino=filter_termino)
            else:
                if id == 'add':
                    if request.method == 'GET':
                        form = forms.medioexcluyenteForm(request.form)
                        medio_excluyente = None
                        return render_template('medio_excluyente/form.html', termino= medio_excluyente, user= u, form = form)
                    if request.method == 'POST':
                        form = forms.medioexcluyenteForm(request.form)
                        medio =form.medio.data
                        terminos =form.termino.data
                        
                        ins_modif_medio_excluyente(medio, terminos)

                        #Ahora actualiza todas las noticias con ese termino
                        return redirect(url_for('get_medio_excluyente'))
                
                medio_excluyente = MedioExcluyente.query.filter_by(id=id).first()
                if request.method == 'GET':
                    
                    if medio_excluyente == None:
                        return 'No existen medios excluentes asociados'
                    else:
                        form = forms.medioexcluyenteForm(request.form)

                        return render_template('medio_excluyente/form.html', termino= medio_excluyente, user= u, form = form)
                if request.method == 'POST':
                    form = forms.medioexcluyenteForm(request.form)
                    medio =form.medio.data
                    terminos =form.terminos.data
                    ins_modif_medio_excluyente(medio, terminos, medio_excluyente.id)
                        
                    return redirect(url_for('get_medio_excluyente'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/categoria/borrar/<int:id>/')
def borrar_categoria(id):
    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:
            categoria = Categoria.query.filter_by(id=id).first()
            db.session.delete(categoria)
            db.session.commit()
            return redirect(url_for('get_categoria'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/categoria/')
@app.route('/categoria/<id>/',  methods=['GET', 'POST'])
def get_categoria(id = ''):

    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:

            if id == '':
                categoria = Categoria.query
                filter_id = request.args.get('filter_id', '')
                filter_nombre= request.args.get('filter_nombre', '')
                
                if filter_nombre != '':
                    categoria = categoria.filter(Categoria.nombre_categoria.like('%'+filter_nombre+'%'))
                if filter_id != '':
                    categoria = categoria.filter_by(id=filter_id)

                if categoria == None:
                    return 'No existen categorias asociadas'
                else:
                    return render_template('categoria/lista.html', terminos= categoria, user= u, filter_id = filter_id, filter_nombre=filter_nombre)
            else:
                if id == 'add':
                    if request.method == 'GET':
                        form = forms.categoryForm(request.form)
                        termino = None
                        return render_template('categoria/form.html', termino= termino, user= u, form = form)
                    if request.method == 'POST':
                        form = forms.categoryForm(request.form)
                        nombre_cat =form.nombre.data
                        ins_modif_categoria(nombre_cat)
                        return redirect(url_for('get_categoria'))
                
                termino = Categoria.query.filter_by(id=id).first()
                if request.method == 'GET':
                    
                    if termino == None:
                        return 'No existen noticias asociadas'
                    else:
                        form = forms.categoryForm(request.form)
                        return render_template('categoria/form.html', termino= termino, user= u, form = form)
                if request.method == 'POST':
                    form = forms.categoryForm(request.form)
                    nombre_cat =form.nombre.data
                    ins_modif_categoria(nombre_cat, termino.id)
                    return redirect(url_for('get_categoria'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/medio/borrar/<int:id>/')
def borrar_medio(id):
    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:

            medio = Medio.query.filter_by(id=id).first()
            db.session.delete(medio)
            db.session.commit()
            return redirect(url_for('get_medio'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/medio/')
@app.route('/medio/<id>/',  methods=['GET', 'POST'])
def get_medio(id = ''):

    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:

            if id == '':
                medio = Medio.query
                filter_id = request.args.get('filter_id', '')
                filter_nombre= request.args.get('filter_nombre', '')
                
                if filter_nombre != '':
                    medio = medio.filter(Medio.nombre.like('%'+filter_nombre+'%'))
                if filter_id != '':
                    medio = medio.filter_by(id=filter_id)

                if medio == None:
                    return 'No existen medios asociados'
                else:
                    return render_template('medio/lista.html', terminos= medio, user= u, filter_id = filter_id, filter_nombre=filter_nombre)


            else:
                if id == 'add':
                    if request.method == 'GET':
                        form = forms.medioForm(request.form)
                        medio = None
                        return render_template('medio/form.html', termino= medio, user= u, form = form)
                    if request.method == 'POST':
                        form = forms.medioForm(request.form)
                        nombre =form.nombre.data
                        url =form.url.data
                        ins_modif_medio(nombre, url)
                        return redirect(url_for('get_medio'))
                
                medio = Medio.query.filter_by(id=id).first()
                if request.method == 'GET':
                    
                    if medio == None:
                        return 'No existen medios asociados'
                    else:
                        form = forms.medioForm(request.form)
                        return render_template('medio/form.html', termino= medio, user= u, form = form)
                if request.method == 'POST':
                    form = forms.medioForm(request.form)
                    nombre =form.nombre.data
                    url =form.url.data
                    ins_modif_medio(nombre, url, medio.id)
                    return redirect(url_for('get_medio'))
        else:
            return redirect(url_for('index', mensaje="1"))


@app.route('/imagen/borrar/<int:id>/')
def borrar_imagenes(id):
    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:
            termino = Foto.query.filter_by(idfotos=id).first()
            db.session.delete(termino)
            db.session.commit()
            return redirect(url_for('get_imagenes'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/imagen/')
@app.route('/imagen/<id>/',  methods=['GET', 'POST'])
def get_imagenes(id = ''):

    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:

            if id == '':
                termino = Foto.query
                filter_id = request.args.get('filter_id', '')
                filter_nombre= request.args.get('filter_nombre', '')
                filter_categoria= request.args.get('filter_categoria', '')
                
                if filter_categoria != '':
                    termino = termino.join(Categoria).filter(Categoria.nombre_categoria.like('%'+filter_categoria+'%'))                
                if filter_nombre != '':
                    termino = termino.filter(Foto.url.like('%'+filter_nombre+'%'))
                if filter_id != '':
                    termino = termino.filter_by(idfotos=filter_id)

                if termino == None:
                    return 'No existen imagenes asociadas'
                else:
                    return render_template('imagen/lista.html', terminos= termino, user= u, filter_id = filter_id, filter_nombre=filter_nombre, filter_categoria=filter_categoria)
            else:
                if id == 'add':
                    if request.method == 'GET':
                        form = forms.imagenForm(request.form)
                        

                        termino = None
                        return render_template('imagen/form.html', termino= termino, user= u, form = form)
                    if request.method == 'POST':
                        form = forms.imagenForm(request.form, request.files)

                        f = request.files['foto']
                        url =f.filename
                        extension = url.split(".")[1]
                        if extension == 'zip':
                            archive = ZipFile(f, 'r')
                            files = archive.namelist()
                            for arch in files:
                                foto = archive.read(arch)
                                
                                print(arch)
                                if foto != '' and foto != None:
                                    ins_modif_imagen(arch.split('/')[1], foto, None)
                        else:

                            foto = f.read()
                            categorias =form.categorias.data

                            #filename = secure_filename(f.filename)

                            #f.save(os.path.join(
                            #    '', 'uploads', filename
                            #))
                        


                            ins_modif_imagen(url, foto, categorias)
                        return redirect(url_for('get_imagenes'))
                
                fotoarch = Foto.query.filter_by(idfotos=id).first()
                if request.method == 'GET':
                    
                    if fotoarch == None:
                        return 'No existen terminos asociados'
                    else:
                        form = forms.editimagenForm(request.form, request.files)
                        ruta = base64.b64encode(fotoarch.foto).decode()
                        return render_template('imagen/form.html', termino= fotoarch, user= u, form = form, ruta=ruta)
                if request.method == 'POST':
                    form = forms.editimagenForm(request.form, request.files)
                    try:
                        f = request.files['foto']
                        url =f.filename
                        foto = f.read()
                    except BadRequestKeyError:
                        url =None
                        foto = None
                    categorias =form.categorias.data
                    ins_modif_imagen(url, foto, categorias, fotoarch.idfotos)
                        
                    return redirect(url_for('get_imagenes'))
        else:
            return redirect(url_for('index', mensaje="1"))
                
@app.route('/noticia/')
@app.route('/noticia/<campo>/<id>/')
@app.route('/noticia/<int:id>/')
def get_historico(campo = '', id = ''):

    if 'user' in session:
        user = session['user']

    historico = Historico.query.order_by(Historico.fecha.desc())

    user = User.query.filter_by(user=user).first()    


    ret = ''
    if id == '':
        historico = Historico.query.order_by(Historico.fecha.desc())
        historico = historico.filter(Historico.termino.isnot(None))
        if user.nivel != 100:
            lista = list()
            lista = user.termino.split(',')
            historico = historico.filter(Historico.termino.in_(lista))

        if historico.count() == 0:
            return 'No existen noticias asociadas'
        else:
            ret = ''
            for h in historico:
                if h.get_termino() != None:
                    ret += str('{}'.format(h.idhistoricos))+'<br> {}'.format(h.titulo)+'<br> {}'.format(h.bajadaNoticia)+'<br> '+'<br> {}'.format(h.link)+'<br> {}'.format(h.link_foto)+'<br> {}'.format(h.get_medio())+'<br> {}'.format(h.get_termino())+'<br> {}'.format(h.fecha)+'<hr>'
            return ret
            if ret == '':
                return 'No existen noticias'
            else:
                return ret

    if id != '' and campo != '':
        if campo == 'id':
            campo = 'idhistoricos'
        elif campo == 'medio' or campo == 'medioOrigen':
            campo = 'medioOrigen'
            
            if id.isdigit() == False:
                historico = historico.join(Medio).filter(Medio.nombre.like('%'+str(id)+'%'))
            
            else:
                kwargs = {campo: str(id)}
                historico = historico.filter_by(**kwargs)

        elif campo == 'termino':
            
            if id.isdigit() == False:
                historico = historico.join(Termino).filter(Termino.nombre.like('%'+str(id)+'%'))
            
            else:
                kwargs = {campo: str(id)}
                historico = historico.filter_by(**kwargs)

        elif campo == 'fecha':
            
            historico = historico.filter(Historico.fecha.like('%'+id+'%'))

        elif campo == 'usuario':
            user = User.query.filter_by(user=str(id)).first()
            lista = list()
            lista = user.termino.split(',')
            historico = historico.filter(Historico.termino.in_(lista))

            #historico = historico.filter(Historico.fecha.like( str(datetime.date.today()) ))



    
        else:
            kwargs = {campo: str(id)}
            historico = historico.filter_by(**kwargs)
        if historico.count() == 0:
            return 'No existen noticias asociadas'
        else:
            historico = historico.filter(Historico.termino.isnot(None))
            for h in historico:
                if h.link_foto == '' or link_foto == None:
                    data = asignar_imagen(historico, h.get_categoria(), h.idhistoricos)
                    if data != None:
                        #data = Foto.query.filter_by(idfotos = h.link_foto).first()
                        image = base64.b64encode(data.foto).decode()
                        aux = '<img src="data:image/png;base64,'+str('{}'.format(image))+'" width="60%">'
                        #aux = "<img 'data:image/png;base64,"+str(image)+"' width='60%'>"
                    else:
                        aux = ''    

                ret += str('{}'.format(h.idhistoricos))+'<br> {}'.format(h.titulo)+'<br> {}'.format(h.bajadaNoticia)+'<br>'+str(aux)+'<br> {}'.format(h.link_foto)+'<br> {}'.format(h.get_medio())+'<br> {}'.format(h.get_termino())+'<br> {}'.format(h.get_categoria())+'<br> {}'.format(h.fecha)+'<hr>'
            return ret
            
@app.route('/user/borrar/<int:id>/')
def borrar_user(id):
    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:
            user = User.query.filter_by(id=id).first()
            db.session.delete(user)
            db.session.commit()
            return redirect(url_for('get_user'))
        else:
            return redirect(url_for('index', mensaje="1"))

@app.route('/user/')
@app.route('/user/<id>/',  methods=['GET', 'POST'])
def get_user(id = ''):

    if 'user' in session:
        user = session['user']
        u = User.query.filter_by(user=user).first()
        if u.is_admin() == True:

            if id == '':
                usuario = User.query
                filter_id = request.args.get('filter_id', '')
                filter_nombre= request.args.get('filter_nombre', '')
                mensaje = request.args.get('mensaje', '')

                if filter_nombre != '':
                    usuario = usuario.filter_by(user=filter_nombre)
                if filter_id != '':
                    usuario = usuario.filter_by(id=filter_id)

                if usuario == None:
                    return 'No existen usarios asociados'
                else:
                    return render_template('usuario/lista.html', terminos= usuario, user= u, filter_id = filter_id, filter_nombre=filter_nombre, mensaje=mensaje)
            else:
                if id == 'add':
                    if request.method == 'GET':
                        form = forms.userForm(request.form)
                        usuario = None
                        return render_template('usuario/form.html', termino= usuario, user= u, form = form)
                    if request.method == 'POST':
                        form = forms.userForm(request.form)
                        nombre =form.nombre.data
                        passwd =form.passwd.data
                        terminoform =request.form.getlist("termino")
                        ins_modif_user(nombre, terminoform, passwd)
                        
                        return redirect(url_for('get_user'))
                
                usuario = User.query.filter_by(id=id).first()
                if request.method == 'GET':
                    
                    if usuario == None:
                        return 'No existen usuarios asociados'
                    else:
                        form = forms.usermodifForm(request.form)
                        aux = usuario.termino
                        dic_terminos=[]
                        if aux != None: 
                            for a in aux.split(','):
                                term = Termino.query.filter_by(idterminos=a).first()
                                dic_terminos.append(term)

                        return render_template('usuario/form.html', termino= usuario, user= u, form = form, dic_terminos = dic_terminos)
                if request.method == 'POST':
                    form = forms.usermodifForm(request.form)
                    nombre =form.nombre.data
                    terminoform =request.form.getlist("termino")
                    passwd =form.passwd.data
                    try:
                        ins_modif_user(nombre, terminoform, passwd, id)
                        return redirect(url_for('index'))   
                    except NameError:
                        return redirect(url_for('get_user', mensaje="2"))
        else:
            return redirect(url_for('index', mensaje="1"))


        '''
        else:
            
            if request.method == 'GET':
                form = forms.usermodifForm(request.form)
                aux = u.termino
                dic_terminos=[]
                if aux != None: 
                    for a in aux.split(','):
                        term = Termino.query.filter_by(idterminos=a).first()
                        dic_terminos.append(term)

                return render_template('usuario/form.html', termino= u, user= u, form = form, dic_terminos = dic_terminos)
            if request.method == 'POST':
                form = forms.usermodifForm(request.form)
                nombre =form.nombre.data
                terminoform =request.form.getlist("termino")
                passwd =form.passwd.data
                ins_modif_user(nombre, terminoform, passwd, u.id)
                return redirect(url_for('index'))            
        '''

@csrf.exempt
@app.route('/api/noticias/', methods=['GET', 'POST'])
def get_api():
    if request.method == 'POST':
        try:
            nombre = str(request.values['nombre'])
        except KeyError:
            response = app.response_class(
            response=json.dumps([{"estado": "error", "codigo": 400, "mensaje":"Falta el campo 'nombre'"}]),
            status=400,
            mimetype='application/json'
            )
            return response
        try:
            passwd = str(request.values['password'])
        except KeyError:
            response = app.response_class(
            response=json.dumps([{"estado": "error", "codigo": 400, "mensaje":"Falta el campo 'password'"}]),
            status=400,
            mimetype='application/json'
            )
            return response
        try:
            tabla = str(request.values['tabla'])
        except KeyError:
            tabla =''
            pass
        try:
            dato = str(request.values['dato'])#puede ser string o int
        except KeyError:
            dato=''
            pass

        res = []
           

        user = User.query.filter_by(user=nombre).first()
        if user is not None and user.verify_password(passwd):
            noticias = Historico.query.order_by(Historico.fecha.desc())
            
            if user.nivel != 100:

                lista = list()
                lista = user.termino.split(',')
                noticias = noticias.filter(Historico.termino.in_(lista))


            #kwargs = {campo: str(id)}
            #noticias = historico.filter_by(**kwargs)    

            if tabla == 'termino':
                if dato.isdigit():
                    noticias = noticias.filter(Historico.termino.like('%'+str(dato)+'%'))
                    
                    #kwargs = {campo: str(dato)}
                    #noticias = noticias.filter_by(**kwargs)
                else:

                    t = Termino.query.filter( Termino.nombre.like('%'+str(dato)+'%') ).first()
                    noticias = noticias.filter(Historico.termino.like('%'+str(t.idterminos)+'%'))
                    
                    #noticias = noticias.join(Termino).filter(Termino.nombre.like('%'+str(dato)+'%'))
                    
            elif tabla == 'categoria':
                terms = Termino.query.join(Categoria).filter(Categoria.nombre_categoria.like('%'+str(dato)+'%'))

                lista_terms = list()
                for t in terms:
                    lista_terms.append(t.idterminos)

                    #noticias = noticias.filter(Historico.termino.like('%'+str(terms.idterminos)+'%'))
                
                noticias = noticias.filter(Historico.termino.in_(lista_terms))

                #noticias = noticias.join(Termino).filter( )


            elif tabla == 'medio':
                tabla = 'medioOrigen'
                if dato.isdigit():
                    kwargs = {tabla: str(dato)}
                    noticias = noticias.filter_by(**kwargs)
                else:
                    noticias = noticias.join(Medio).filter(Medio.nombre.like('%'+str(dato)+'%'))
                
            elif tabla == 'titulo':
                noticias = noticias.filter(Historico.titulo.like('%'+str(dato)+'%'))

            elif tabla == 'fecha':
                noticias = noticias.filter(Historico.fecha.like('%'+str(dato)+'%'))
        
            for h in noticias:
                t = Termino.query.filter_by(idterminos=h.termino).first()
                aux = {}
                aux['id'] = str('{}'.format(h.idhistoricos))
                aux['titulo'] = str('{}'.format(h.titulo))
                aux['bajada'] = str('{}'.format(h.bajadaNoticia))
                aux['link'] = str('{}'.format(h.link))
                if h.link_foto == None or link_foto == '':
                    data = asignar_imagen(historico, h.get_categoria(), h.idhistoricos)
                    if data != None:
                        image = base64.b64encode(data.foto).decode()
                        auxtxt = '<img src="data:image/png;base64,'+str('{}'.format(image))+'" width="60%">'
                        
                        aux['link_foto'] = auxtxt
                    #data = Foto.query.filter_by(idfotos = h.link_foto)
                    #aux['link_foto'] = "<img data:image/png;base64,"+str(data.foto)+">"
                else:
                    aux['link_foto'] = str('{}'.format(h.link_foto))
                aux['bajada'] = str('{}'.format(h.bajadaNoticia))
                aux['medio'] = str('{}'.format(h.get_medio()))
                aux['termino'] = str('{}'.format(h.get_termino()))
                #aux['termino'] = str('{}'.format(t.get_padre()))
                #aux['categoria'] = str('{}'.format(h.get_categoria()))
                aux['fecha'] = str('{}'.format(h.fecha))
                res.append(aux)

            response = app.response_class(
            response=json.dumps(res,  indent=4),
            status=200,
            mimetype='application/json'
            )
        else:

            response = app.response_class(
            response=json.dumps([{"estado": "error", "codigo": 401, "mensaje":"Error de Autenticación"}]),
            status=401,
            mimetype='application/json'
            )
        return response

@app.route('/scraping/')
def scraping():
    ret = ''
    medios = Medio.query
    for m in medios:
        if m.url != None:
            print(m.id)
            
            if m.id == 2 or m.id==1:
                getHeadlines(m.url)
            if m.id ==3:
                get_emol(m.url)
            if m.id == 5:
                get_bolsa_comercio_esencial(m.url)
            if m.id == 8:
                get_bolsa_comercio_noticias(m.url)
            
            if m.id == 4:
                get_elpulso(m.url)
            
            if m.id == 9:
                get_bbc(m.url)
            
            if m.id == 10:
                get_wsj(m.url)
            
            if m.id == 11:
                get_nytimes(m.url)
            
            ret += str(m.nombre)+"<br>"
    return ret


@app.route('/trigger/termino/')
def trigger_termino():
    historicos = Historico.query
    terminos = Termino.query
    for h in historicos:
        #if h.termino == None or h.termino == '':
        ter = encontrar_termino(h.titulo, h.bajadaNoticia, h.medioOrigen)
        h.termino = ter
        db.session.add(h)
        db.session.commit()                    

    return "Terminos emparejados"

@app.route('/trigger/historico/')
def trigger_historico():
    historicos = Historico.query
    ret = ''
    for h in historicos:
        '''if type(h.categoria) is not int and h.categoria != None:#transformar a indice categoria
            c = Categoria.query.filter_by(nombre_categoria=h.categoria).first()
            if c != None:
                h.categoria = c.id
                db.session.add(h)
                db.session.commit()
        '''
        if type(h.termino) is not int and h.termino != None:#transformar a indice categoria
            t = Termino.query.filter(Termino.nombre.ilike(h.termino)).first()
            if t != None:
                h.termino = t.idterminos
                db.session.add(h)
                db.session.commit()
        if type(h.medioOrigen) is not int and h.medioOrigen != None:#transformar a indice categoria
            m = Medio.query.filter(Medio.nombre.ilike(h.medioOrigen)).first()
            if m != None:
                h.medioOrigen = m.id
                db.session.add(h)
                db.session.commit()


    ret +="Categoria Historicos emparejados<br>"
    ret +="Termino Historicos emparejados<br>"
    ret +="Medio Historicos emparejados<br>"
    return ret


if __name__ == '__main__':
    csrf.init_app(app)
    db.init_app(app)

    #migrate
    with app.app_context():
        db.create_all()
        

    app.run(host="0.0.0.0", port=8000)
    #app.run(port=80)