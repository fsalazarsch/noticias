from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
import datetime


db = SQLAlchemy()

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user= db.Column(db.String(25), unique=True)
	password = db.Column(db.String(255))
	created_time= db.Column(db.DateTime, default=datetime.datetime.now)
	nivel = db.Column(db.Integer, default = 10)
	termino = db.Column(db.Text)

	def __init__(self, username, password):
		self.user= username
		self.password = self.__generate_password(password)
		

	def __generate_password(seld, password):
		return generate_password_hash(password)

	def verify_password(self, password):
		return check_password_hash(self.password, password)
	def is_admin(self):
		if self.nivel == 100:
			return True
		else:
			return False
	def get_terminos(self, mercado):
		#mercado --> 0: cuenta todo, 1: cuenta solo los de mercado, 2: cuenta todos los no mercado
		aux = list()
		indices = self.termino.split(',')
		for i in indices:
			t = Termino.query.filter_by(idterminos=i).first()

			if mercado == 1 and t.categoria == 25:
				aux.append(t.nombre)
			if mercado == 2 and t.categoria != 25:
				aux.append(t.nombre)
			if mercado == 0:
				aux.append(t.nombre)		
		return aux


class Categoria(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	nombre_categoria = db.Column(db.String(255), unique =True)
	noticias = db.relationship('noticia')
	def __str__(self):
		return self.nombre_categoria
	def contar_fotos(self):
		return Foto.query.filter_by(categoria=self.id).count()


class noticia(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	categoria = db.Column(db.Integer, db.ForeignKey('categoria.id'))

class Termino(db.Model):
	idterminos = db.Column(db.Integer, primary_key=True)
	nombre = db.Column(db.String(255))
	padre = db.Column(db.String(45))
	categoria = db.Column(db.Integer, db.ForeignKey('categoria.id'))
	def __str__(self):
		return self.nombre
	def get_padre(self):
		if self.padre == None:
			return '---'
		padre = self.padre.split(",")
		m = Termino.query.filter_by(idterminos=padre[0]).first()
		if m == None:
			return '---'
		return m.nombre
	def get_categoria(self):
		m = Categoria.query.filter_by(id=self.categoria).first()
		if m == None:
			return None
		return m.nombre_categoria

class Excluyente(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	nombre = db.Column(db.String(255))
	termino = db.Column(db.Integer, db.ForeignKey('termino.idterminos'))
	def get_termino(self):
		m = Termino.query.filter_by(idterminos=self.termino).first()
		if m == None:
			return None
		return m.nombre

class MedioExcluyente(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	medioOrigen = db.Column(db.Integer, db.ForeignKey('medio.id'))
	termino = db.Column(db.Integer, db.ForeignKey('termino.idterminos'))
	def get_medio(self):
		m = Medio.query.filter_by(id=self.medioOrigen).first()
		if m == None:
			return None
		return m.nombre
	def get_termino(self):
		m = Termino.query.filter_by(idterminos=self.termino).first()
		if m == None:
			return None
		return m.nombre

class Medio(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	nombre = db.Column(db.String(255))
	url = db.Column(db.Text)
	#idioma = db.Column(db.Integer)
	def __str__(self):
		return self.nombre
		
class Historico(db.Model):
	__tablename__ = 'historicos'
	idhistoricos = db.Column(db.Integer, primary_key=True)
	titulo = db.Column(db.Text)
	bajadaNoticia = db.Column(db.Text)
	link = db.Column(db.String(255))
	link_foto = db.Column(db.String(255))
	medioOrigen = db.Column(db.Integer, db.ForeignKey('medio.id'))
	termino = db.Column(db.Text)
	fecha = db.Column(db.DateTime, default=datetime.datetime.now)
	def get_medio(self):
		m = Medio.query.filter_by(id=self.medioOrigen).first()
		if m == None:
			return None
		return m.nombre
	def get_categoria(self):
		t = Termino.query.filter_by(idterminos=self.termino).first()
		if t == None:
			return None
		m = Categoria.query.filter_by(id=t.categoria).first()
		if m == None:
			return None
		return m.nombre_categoria
	def get_termino(self):
		m = Termino.query.filter_by(idterminos=self.termino).first()
		if m == None:
			return None
		return m.nombre

		
class Foto(db.Model):
	idfotos = db.Column(db.Integer, primary_key=True)
	url = db.Column(db.String(255))
	foto = db.Column(db.LargeBinary(length=(2**32)-1))
	categoria = db.Column(db.Integer, db.ForeignKey('categoria.id'))
	def get_categoria(self):
		m = Categoria.query.filter_by(id=self.categoria).first()
		if m == None:
			return None
		return m.nombre_categoria





