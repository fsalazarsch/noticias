from bs4 import BeautifulSoup, element, SoupStrainer
import requests, datetime
from insertar import insertar_noticia
from models import db, User, Termino, Excluyente, MedioExcluyente, Historico, Categoria, Foto
import time, json, requests, dateutil, lxml
from dateutil import parser as date_parser

def nro_noticias(noticias, categoria, idnoticia):

    if categoria == None:
        return None
    index = 0
    for n in noticias:
        
        if n.get_categoria() == categoria:
            #noticias = noticias.filter_by(get_categoria=c.id)
            if n.idhistoricos == idnoticia:
                return index
            else:
                index += 1

def nro_imagenes(categoria):
    c = Categoria.query.filter_by(nombre_categoria=categoria).first()
    if c == None:
        return 0  
    return Foto.query.filter_by(categoria=c.id).count()
    

def asignar_imagen(noticias, categoria, idnoticia):
    nro_noticia = nro_noticias(noticias, categoria, idnoticia)
    nro_imagen = nro_imagenes(categoria)
    if nro_imagenes == 0:
        return None

    c = Categoria.query.filter_by(nombre_categoria=categoria).first()
    if c == None:
        return None

    lista_fotos = Foto.query.filter_by(categoria=c.id)
    return lista_fotos[int(nro_noticia)%int(nro_imagen)]




def encontrar_termino(nombre, descripcion, medio= ''):
    ter= None
    terminos = Termino.query
    
    for t in terminos:
        try:
            if t.nombre.lower() in nombre.lower() or t.nombre.lower() in descripcion.lower():            
                ter = t.idterminos
                continue
        except AttributeError:
            return None
    try:       
        excluyentes  = Excluyente.query.filter_by(termino=ter)
        if excluyentes != None:
            for e in excluyentes:
                if e.nombre.lower() in nombre.lower() or e.nombre.lower() in descripcion.lower():            
                    ter = None
        #return ter
    except AttributeError:
        return None
    #vemos si no se puede asignar esta noticia para el término 
    try:
        mediosexcl = MedioExcluyente.query.filter_by(termino=ter, medioOrigen= medio).first()
        if mediosexcl != None:
            #print(mediosexcl)
            return None        
    except AttributeError:
        return None
    return ter

def parseRSS(rss_url):
    return feedparser.parse(rss_url)

def getHeadlines(rss_url):
    terminos = Termino.query
    page = requests.get(rss_url)

    soup = BeautifulSoup(page.content, 'lxml')
    #soup = soup.decode('utf-8', 'ignore')

    if 'elmostrador' in rss_url:
        medioOrigen  = 2
        if soup.find('channel').get_text() != '':
            datos = soup.find('channel')
            noticias = datos.findAll('item')
            for n in noticias:
                titulo = n.find('title').get_text()#title
                descripcion = n.find('description').get_text()#bajada
                link = n.find('link').get_text()#link
                try:
                    fecha = n.find('pubDate').get_text()#link
                    fecha = date_parser.parse(fecha)
                    
                except AttributeError:
                    fecha = datetime.datetime.now()
                if n.find('image'):
                    link_foto = n.find('image').get_text()
                else:
                    link_foto=''
                ter= None
                ter = encontrar_termino(titulo, descripcion, medioOrigen)
                #for t in terminos:
                #    if t.nombre in titulo or t.nombre in descripcion:
                #        ter = t.idterminos                    
                #        pass

                insertar_noticia(titulo, descripcion, link, medioOrigen, ter, fecha, link_foto)
    if 'df' in rss_url:
        medioOrigen  = 1
        if soup.find('channel').get_text() != '':
            datos = soup.find('channel')
            noticias = datos.findAll('item')
            for n in noticias:
                #print(n)
                titulo = n.find('title').get_text()#title
                link = n.find('link').get_text()#link
                descripcion = n.find('description').get_text()#bajada
                ter= None
                ter = encontrar_termino(titulo, descripcion, medioOrigen)
                
                #for t in terminos:
                #    if t.nombre in titulo or t.nombre in descripcion:
                #        ter = t.idterminos                    
                #        continue

                fecha = n.find('df:ts').get_text()
                fecha = str(datetime.datetime.strptime (fecha, '%Y%m%d%H%M%S'))
                link_foto = "www.df.cl"+n.find('df:foto600').get_text()
                if link_foto == "www.df.cl":
                    link_foto = ''

                insertar_noticia(titulo, descripcion, link, str(medioOrigen), str(ter), fecha, link_foto)

def get_bolsa_comercio_esencial(url):
    #url = "http://www.bolsadesantiago.com/labolsa/Paginas/Hechos-Esenciales.aspx?RequestHechosEsenciales=1&hdnPag=1&hdnDia=dia&hdnMes=mes&hdnAno=ano&Nemo="

    res = ''
    req = requests.get(url)
    time.sleep(5)
    statusCode = req.status_code
    if statusCode == 200:

        html = BeautifulSoup(req.text, "html.parser")
        newJson= json.loads(str(html))
        terminos = Termino.query
    
        for n in range(0, 10):
            #print("Indice:"+str(n)+"\n")
            jsonInterno= newJson['ListHechos']
            titulo=newJson['ListHechos'][n]['Titulo']
            urlnot='http://www.bolsadesantiago.com'+newJson['ListHechos'][n]['UrlAdjunto']
            fech=newJson['ListHechos'][n]['FechaString']
            
            #nemo sera nuestro termino si no existe se crea 
            nemo = newJson['ListHechos'][n]['Nemo']
            term = Termino.query.filter_by(nombre= nemo).first()
            if term == None:
                term = Termino(nombre=nemo)
                db.session.add(term)
                db.session.commit()

            fecha = date_parser.parse(fech)
                    
            bajadaNoticia=''
            medioOrigen=5
            
            #res =titulo+", "+bajadaNoticia+", "+urlnot+", "+str(medioOrigen)+", "+str(term.idterminos)+", "+str(fecha)+"<br>"
            insertar_noticia(titulo, bajadaNoticia, urlnot, medioOrigen, term.idterminos, fecha, '')
    #return res 

def get_bolsa_comercio_noticias(url):
    #http://www.bolsadesantiago.com/noticias/Paginas/Datos-Burs%C3%A1tiles-disponibles-en-la-Tienda-Online-de-la-Bolsa-de-Santiago.aspx
    req = requests.get(url)
    time.sleep(5)
    statusCode = req.status_code
    terminos = Termino.query

    if statusCode == 200:
        html = BeautifulSoup(req.text, "html.parser")
        
        ListadoNoticias = html.find(id = 'ListadoNoticias')
        entradas = ListadoNoticias.find_all('div', {'class': 'fila bloqueNoticia'})
            
        for entrada in entradas:
            
            titulo = entrada.find('h3').getText()
            link = entrada.find('a')
            links=link.attrs['href']
            #fecha = entrada.find('span').getText()
            fecha = entrada.find('p').getText()
            fecha = date_parser.parse(fecha)
                    
            link_raw = entrada.find('img')
            link_foto = link_raw.attrs['src']
            for t in Termino.query:
                if t.nombre in titulo:
                    term = t
                else:
                    term = None            
            
            bajadaNoticia=''
            medioOrigen=8
            
            #print(str(titulo.encode('utf-8').decode('utf-8'))+","+bajadaNoticia+","+links+","+str(medioOrigen)+","+str(fecha)+","+str(categoria)+","+str(t.nombre))
            insertar_noticia(titulo, bajadaNoticia, links, medioOrigen, t, fecha, link_foto)

    else:
        pass 

def get_elpulso(urls):
    ret = ""
    url = urls.split(",")
    for u in url:
        for i in range(1, 6):
    
            if i > 1:
                aux_url = "%spage/%d/" % (u, i)
            else:
                aux_url = u
            req = requests.get(aux_url)
            
            statusCode = req.status_code
            if statusCode == 200:

                html = BeautifulSoup(req.text, "lxml")
                #print(html)
                entradas = html.find_all('article', {'class': "border-bottom-1 archive-article"})
               # print(entradas)
               
                for entrada in entradas:
                    #print(entrada)
                    titulo = entrada.find('h4', {'class' : 'normal'}).getText().strip()
                    link = entrada.find('a', {'class' : 'tit-lk'})
                    img = entrada.find('div', {'class' : 'cajaimg img-nota-p'})
                    #fecha = entrada.find('span').getText()
                    links=link.attrs['href']
                    link_foto=img.attrs['style']
                    link_foto = link_foto.replace("background-image: url(","")
                    link_foto = link_foto.replace(");","")
                    term = None
                    for t in Termino.query:
                        if t.nombre in titulo:
                            term = t
                        else:
                            term = None
                    
                    tst = entrada.small.find('span', {'class' : 'ltpicto-time'})
                    tst = tst.next_sibling.strip()

                    tst = datetime.datetime.strptime (tst, '%d/%m/%Y %H:%M %p')
                    #tst = datetime.datetime.strptime(tst.next_sibling, "%d-%m-%Y").strftime("%Y-%m-%d")
                    if tst == None:
                    #for ts in tst:
                    #    ret += "<hr>"+ts+"<hr>"
                    #tst = tst.text
                    #tst = tst_raw.text
                        tst = datetime.datetime.now()
                    bajadaNoticia=''
                    medioOrigen=4
                    
                        
                    #print(str(titulo)+","+bajadaNoticia+","+links+","+link_foto+","+str(medioOrigen)+","+str(tst)+","+str(term)+"<br>")
                    insertar_noticia(titulo, bajadaNoticia, links, medioOrigen, t, tst, link_foto)

            else:
                break

def get_emol(urls):
    url = urls.split(",")
    for u in url:
        req = requests.get(u)
        
        statusCode = req.status_code
        if statusCode == 200:

            html = BeautifulSoup(req.text, "html.parser")
            #print(html)
            entradas = []
            entradas += (html.find_all('div', {'class': 'col_center_noticia2-390px'}))
            entradas += (html.find_all('div', {'class': 'col_center_noticia1-390px'}))
            entradas += (html.find_all('div', {'class': 'col_center_noticia4dest-360px bor_destacado'}))
            entradas += (html.find_all('div', {'class': 'col_center_finanzas_item'}))
            entradas += (html.find_all('div', {'class': 'titulo_tend1'}))
            entradas += (html.find_all('div', {'class': 'titulo_tend3'}))


            for entrada in entradas:
                try:
                    titulo = entrada.find('h3').getText()
                except AttributeError:
                    try:
                        titulo = entrada.find('h1').getText()
                    except AttributeError:
                        titulo = ''
                
                link = entrada.find('a')
                try:
                    bajada = entrada.find('p').getText()
                except KeyError:
                    bajada = ""
                except AttributeError:
                    bajada = ""
                try:
                    links=link.attrs['href']
                except KeyError:
                    links =""
                
                #terminos = Termino.query
                medioOrigen=3
                ter= None
                ter = encontrar_termino(titulo, bajada, medioOrigen)
                
                #for t in Termino.query:
                #    if t.nombre in titulo:
                #        term = t

                fecha = datetime.datetime.now()
                
                #print (titulo)
                #print('http://www.emol.com'+links)
                link_foto=''
                #print(str(titulo)+","+bajada+","+links+","+str(medioOrigen)+","+str(fecha)+","+str(term))
                insertar_noticia(titulo, bajada, links, medioOrigen, ter, fecha, link_foto)

        else:
            break

def get_bbc(urls):
    ret = ""
    url = urls.split(",")
    terminos = Termino.query
    medioOrigen  = 9

    for u in url:
        page = requests.get(u)
        soup = BeautifulSoup(page.content, 'html.parser')
        if soup.find('channel') != None:
            noticias = soup.findAll('item')
            for n in noticias:
                titulo = n.find('title').get_text()#title
                descripcion = n.find('description').get_text()#bajada
                link = n.find('link')#link
                #links=link.attrs['href']
                fecha = n.find('pubdate').get_text()#link
                fecha = date_parser.parse(fecha)
                if fecha == None:
                    fecha = datetime.datetime.now()
                link_foto = n.find('media:thumbnail')
                link_foto= link_foto.attrs['url']
                ter= None
                ter = encontrar_termino(titulo, descripcion, medioOrigen)
                
                #for t in terminos:
                #    if t.nombre in titulo or t.nombre in descripcion:
                #        ter = t.idterminos                    
                #        pass
                if titulo:
                    #ret +=str(fecha)+" - "+str(link)+" - "+str(link_foto)+"<br>"
                    insertar_noticia(titulo, descripcion, link, medioOrigen, ter, fecha, link_foto)

def get_wsj(urls):
    ret = ""
    url = urls.split(",")
    terminos = Termino.query
    medioOrigen  = 10

    for u in url:
        page = requests.get(u)
        soup = BeautifulSoup(page.content, 'html.parser')
        if soup.find('channel').get_text() != None or soup.find('channel').get_text() != '':
            noticias = soup.findAll('item')
            for n in noticias:
                titulo = n.find('title').get_text()#title
                descripcion = n.find('description').get_text()#bajada
                link = n.find('link').get_text()#link
                
                fecha = n.find('pubdate').get_text()#link
                fecha = date_parser.parse(fecha)
                if fecha == None:
                    fecha = datetime.datetime.now()
                

                link_foto = n.find('media:content')
                if link_foto != None:
                    link_foto= link_foto.url#['url']
                
                ter= None
                ter = encontrar_termino(titulo, descripcion, medioOrigen)
                
                #for t in terminos:
                #    if t.nombre in titulo or t.nombre in descripcion:
                #        ter = t.idterminos                    
                #        pass
                #ret +=str(fecha)+" === "+str(titulo)+" == "+str(link_foto)+"<br>"
                insertar_noticia(titulo, descripcion, link, medioOrigen, ter, fecha, link_foto)

def get_nytimes(urls):
    ret = ""
    url = urls.split(",")
    terminos = Termino.query
    medioOrigen  = 11

    for u in url:
        page = requests.get(u)
        soup = BeautifulSoup(page.content, 'html.parser')
        if soup.find('channel').get_text() != None or soup.find('channel').get_text() != '':
            noticias = soup.findAll('item')
            for n in noticias:
                titulo = n.find('title').get_text()#title
                descripcion = n.find('description').get_text()#bajada
                link = n.find('link').get_text()#link
                
                fecha = n.find('pubdate').get_text()#link
                if fecha == '':
                    fecha = datetime.datetime.now()
                else:
                    fecha = datetime.datetime.strptime (fecha, '%a, %d %b %Y %H:%M:%S %Z')
                #datetime.datetime.strftime(fecha, "%d %b %Y %H %M %S")
                #fecha = dateutil.parser.parse(fecha)
                

                link_foto = n.find('media:content')
                if link_foto != None:
                    link_foto= link_foto.url#['url']
                ter= None
                ter = encontrar_termino(titulo, descripcion, medioOrigen)
                
                #for t in terminos:
                #    if t.nombre in titulo or t.nombre in descripcion:
                #        ter = t.idterminos                    
                #        pass
                #ret +=str(fecha)+" === "+str(titulo)+" == "+str(link_foto)+"<br>"
                insertar_noticia(titulo, descripcion, link, medioOrigen, ter, fecha, link_foto)


